<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:url var= "logout" value="/Logout" />


<h3 class="text-muted">ESPACE ETUDIANT
    <span class="label label-success">
        <c:if test="${!empty sessionScope.user}">
            Bienvenue ${sessionScope.user}</span>
        <span>
            <button type="button" class="btn btn-default btn-lg">
                <a href="${logout}"><span class="glyphicon glyphicon-user"> </span> Deconnexion</a>
            </button>
        </span>

    </c:if>
</h3>
<nav class="nav nav-justified" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <c:url value="/ListeEtudiant" var="accueil">
            <c:param name="nav" value="homeStudent" />
        </c:url>
        <a class="navbar-brand" href="${accueil}">Accueil</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">

            <li class="">
             <c:forEach items="${infoEtudiant}" var="nextInfos"> 
                 <c:set  var="nom" value="${nextInfos.value.nom}" />
                 <c:set var="prenom" value="${nextInfos.value.prenom}" />
             </c:forEach> 
                <c:url value="/ListeEtudiant" var="displayCoursEtudiant">
                    <c:param name="nav" value="displayAllCoursEtudiant" />
                    <c:param name="nom" value="${nom}" />
                    <c:param name="prenom" value="${prenom}" />
                    <c:param name="user" value="${sessionScope.user}" />
                </c:url>
                <a  class="navbar-brand" href="${displayCoursEtudiant}" >Consultations des cours </a>
            </li>
            <li class="dropdown">
               
                <c:url value="/ListeEtudiant" var="displayAbsenceEtudiant">
                    <c:param name="nav" value="displayAllAbsenceEtudiant" />
                    <c:param name="nom" value="${nom}" />
                    <c:param name="prenom" value="${prenom}" />
                    <c:param name="user" value="${sessionScope.user}" />
                    
                </c:url>
                <a class="navbar-brand" href="${displayAbsenceEtudiant}" >Consultations des absences </a>
                <ul class="dropdown-menu">

                </ul>
            </li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>