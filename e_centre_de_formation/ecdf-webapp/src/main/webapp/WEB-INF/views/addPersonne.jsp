<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:url var= "accueil" value="/HomeServlet" />
<c:url var= "addPersonne" value="/AddPersonneServlet"/>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title>site centre de formation</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/justified-nav.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

    </head>

    <body>

        <div class="container">

            <div class="masthead">

                <!-- Menu navigation -->
                <jsp:include page="/WEB-INF/views/common/menuAdmin.jsp" flush="true"/>
                <!-- Fin menu navigation -->
                <div class="panel-heading"></div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">PERSONNE : AJOUT</div>
                <div class="panel-body">
                    <form class="form-horizontal" action="${addPersonne}" method ="POST" role="form">
                        <div class="form-group">
                            <label for="inputText3" class="col-sm-2 control-label">Nom</label>
                            <div class="col-sm-10">
                                <input type="text" name="nom" class="col-xs-4" placeholder="Text input">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-sm-2 control-label">Prenom</label>
                            <div class="col-sm-10">
                                <input type="text" name="prenom" class="col-xs-4" placeholder="Text input">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-sm-2 control-label">Date de naissance</label>
                            <div class="col-sm-10">
                                <input type="text" name="dateDeNaissance" class="col-xs-4" placeholder="Text input">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-sm-2 control-label">Adresse</label>
                            <div class="col-sm-10">
                                <input type="text" name="adresse" class="col-xs-4" placeholder="Text input">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-sm-2 control-label">Code postal</label>
                            <div class="col-sm-10">
                                <input type="text" name="codePostal" class="col-xs-2" placeholder="Text input">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-sm-2 control-label">Ville</label>
                            <div class="col-sm-10">
                                <input type="text" name ="ville" class="col-xs-3" placeholder="Text input">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" name ="email" class="col-xs-4" id="inputEmail3" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-sm-2 control-label">Photo</label>
                            <div class="col-sm-10">
                                <input type="file" id="photoInputFile">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-sm-2 control-label">Utilisateur</label>
                            <div class="col-sm-10">
                                <input type="text" name ="utilisateur" class="col-xs-4" placeholder="Text input">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" name="password" class="col-xs-4" id="inputPassword3" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputSelect" class="col-sm-2 control-label">Role</label>
                            <div class="col-sm-10">
                                <select class="col-xs-3" name="roleSelect">
                                    <option>Admin</option>
                                    <option>Enseignant</option>
                                    <option>Etudiant</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-10">
                                <button type="submit" class="btn btn-warning">Valider</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Site footer -->
            <div class="footer">
                <p></p>
            </div>

        </div> <!-- /container -->
        <script>
            function valider()
            {
                var lbOK = true;
                var tElements = document.getElementsByTagName("input");
                for (var i = 0; i < tElements.length; i++)
                {
                    if (tElements[i].type === "text" && tElements[i].value === "")
                        lbOK = "Tous les champs sont obligatoires";
                    if (tElements[i].type === "password" && tElements[i].value === "")
                        lbOK = "Tous les champs sont obligatoires";
                    if (tElements[i].type === "checkbox" && !tElements[i].checked)
                        lbOK = "Tous les champs sont obligatoires";
                }

                var tElementsSelect = document.getElementsByTagName("select");
                for (var i = 0; i < tElementsSelect.length; i++)
                {
                    if (tElementsSelect[i].value === "")
                        lbOK = "Tous les champs sont obligatoires";
                }

                return lbOK;
            }
            
        </script>
        <script>    
           window.onload = function()
            {
                 document.getElementById("bt_valider").onclick = function()
                {
                    var r = valider();
                    document.getElementById("error_message").innerHTML = r;
                };
            }
        </script>
         <label id="error_message"></label>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.js"></script>
    </body> 
</html>
