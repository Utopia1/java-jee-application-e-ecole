<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:url var= "accueil" value="/HomeServlet" />
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title>site centre de formation</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/justified-nav.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

    </head>

    <body>

        <div class="container">

            <div class="masthead">
                <!-- Menu navigation -->
                <jsp:include page="/WEB-INF/views/common/menuAdmin.jsp" flush="true"/>
                <!-- Fin menu navigation -->
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="panel-heading"></div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">PROMOTION : LISTE</div>
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Libelle</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${listeDePromotion}" var="nextPromotion">
                                        <tr><td>${nextPromotion.libelle}</td>
                                            <td>
                                                <c:url value="/ListeAdmin" var="promoURL"> 
                                                    <c:param name="nav" value="displayAllPromotion" />
                                                    <c:param name="promo" value="${nextPromotion.libelle}" /> 
                                                </c:url>
                                                <a href="${promoURL}"><img src="images/editer.png" title="Voir promo" height="30px"/></a>
                                            </td>
                                        </tr>
                                    </c:forEach>   
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <br />
                <div class="col-md-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading">INFORMATIONS</div>
                        <div class="panel-body">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Site footer -->
            <div class="footer">
                <p></p>
            </div>

        </div> <!-- /container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.js"></script>
    </body> 
</html>
