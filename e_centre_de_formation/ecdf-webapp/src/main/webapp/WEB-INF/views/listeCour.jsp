<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:url var= "accueil" value="/HomeServlet" />
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title>site centre de formation</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/justified-nav.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

    </head>

    <body>

        <div class="container">

            <div class="masthead">
                <!-- Menu navigation -->
                <jsp:include page="/WEB-INF/views/common/menuAdmin.jsp" flush="true"/>
                <!-- Fin menu navigation -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel-heading"></div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">COURS : LISTE</div>
                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Date debut</th>
                                            <th>Durée</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${listeDeCour}" var="nextCours">
                                            <tr>
                                                <td><fmt:formatDate value="${nextCours.date}" dateStyle="full" /></td>
                                                <td><fmt:formatNumber value="${nextCours.dureeEnMinutes}" type="number" pattern="# ### heures"/></td>
                                                <td>${nextCours.description}</td>
                                                <td>
                                                    <c:url value="/ListeAdmin" var="coursURL">
                                                        <c:param name="nav" value="displayAllCours" />
                                                        <c:param name="cours" value="${nextCours.description}" /> 
                                                    </c:url>
                                                    <a href="${coursURL}"><img src="images/editer.png" title="Voir cours" height="30px"/></a>
                                                </td>
                                            </tr>
                                        </c:forEach>   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">INFORMATIONS</div>
                            <div class="panel-body">
                            </div>
                        </div>
                    </div>

                </div>

                <!-- Site footer -->
                <div class="footer">
                    <p></p>
                </div>

            </div> <!-- / -->
            </div> <!-- /container -->


            <!-- Bootstrap core JavaScript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.js"></script>
    </body> 
</html>
