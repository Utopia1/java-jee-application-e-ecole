<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:url var= "accueil" value="/HomeServlet" />
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title>site centre de formation</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/justified-nav.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

    </head>

    <body>

        <div class="container">

            <div class="masthead">
                <!-- Menu navigation -->
                <jsp:include page="/WEB-INF/views/common/menuEtudiant.jsp" flush="true"/>
                <!-- Fin menu navigation -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel-heading"></div>
                        <div class="panel panel-primary">
                            <div class="panel-heading">MATIERE : LISTE</div>
                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Nom</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${listeMatieresCours}" var="listMatiere">
                                               
                                            <tr>
                                                <td>${listMatiere.nom}</td>
                                                <td>
                                                    <c:url value="/ListeEtudiant" var="coursURL">
                                                        <c:param name="nav" value="displayAllCoursEtudiant" />
                                                        <c:param name="matiere" value="${listMatiere.nom}" /> 
                                                    </c:url>
                                                    <a href="${coursURL}"><img src="images/editer.png" title="Voir details du matiere" height="30px"/></a>
                                                </td>
                                            </tr>
                                            </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="col-md-8">
                        <div class="panel panel-primary">
                            <div class="panel-heading">COURS : LISTE</div>
                            <div class="panel-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                             <th>Matiere</th>
                                            <th>Date debut</th>
                                            <th>Durée</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${listeMatieresCours}" var="nextCours">
                                            <c:forEach items="${nextCours.listCours}" var="nextItemsCours">

                                            <tr>
                                                <td><fmt:formatDate value="${nextItemsCours.value.date}" dateStyle="full" /></td>
                                                <td><fmt:formatNumber value="${nextItemsCours.value.dureeEnMinutes}" type="number" pattern="# ### minutes"/></td>
                                                <td>${nextItemsCours.value.description}</td>
                                                <td>
                                                    <c:url value="/ListeEtudiant" var="coursURL">
                                                        <c:param name="nav" value="displayAllContentCoursPdf" />
                                                        <c:param name="cours" value="${nextItemsCours.value.description}" /> 
                                                    </c:url>
                                                    <a href=javascript:void(0);
                                                       onClick=window.open('${coursURL}','Proprietes','menubar=no,toolbar=no,status=no,scrollbars=no,resizable=yes','width=1200,height=650'); >
                                                        <img src="images/pdf.jpg" title="Voir details du cours"/></a>
                                                </td>
                                            </tr>
                                            </c:forEach>
                                        </c:forEach>   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- Site footer -->
                <div class="footer">
                    <p></p>
                </div>

            </div> <!-- / -->
        </div> <!-- /container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.js"></script>
    </body> 
</html>
