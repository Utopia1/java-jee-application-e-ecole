<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix= "fmt" uri="http://java.sun.com/jstl/fmt" %>
<c:url var= "login" value="/LoginServlet" />
<c:url var= "listeEtudiant" value="/ListeEtudiant" />
<c:url var= "listeEnseignant" value="/ListeEnseignant" />
<c:url var= "listeAdmin" value="/ListeAdmin" />
<c:url var= "connexion" value="/Connexion" />
<c:url var= "logout" value="/Logout" />
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../assets/ico/favicon.png">

        <title>site centre de formation</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/justified-nav.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">


    </head>

    <body>

        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">PROJET E-ECOLE</a>
            </div>
            <div class="navbar-collapse collapse">

                <c:if test="${!empty sessionScope.user}">
                    <span class="label label-success">
                        Bienvenue ${sessionScope.user}</span>
                    <span>
                        <button type="button" class="btn btn-default btn-lg">
                            <a href="${logout}"><span class="glyphicon glyphicon-user"> </span> Deconnexion</a>
                        </button>
                    </span>
                </c:if>
                <c:if test="${empty sessionScope.user}">
                    <form class="navbar-form navbar-right" method="POST" action="${connexion}">
                        <div class="form-group">
                            <input type="login" name="login" placeholder="Login" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" placeholder="Password" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-success">Se connecter</button>
                        <c:if test="${!empty param}">
                            <c:if test="${empty param['login']}">
                                <p class='alert-danger'>
                                    <c:out  value="Veuillez saisir votre nom d'utilisateur !!" />
                                </p>
                            </c:if>
                            <c:if test="${empty param['password']}">
                                <p class='alert-danger'>
                                    <c:out  value="Veuillez saisir votre mot de passe !!" />
                                </p>
                            </c:if>
                        </c:if>
                    </form>
                </c:if>
            </div><!--/.navbar-collapse -->

            <!-- Jumbotron -->
            <div class="jumbotron">
                <h1>Bienvenue sur e-ecole</h1>
                <p class='alert-danger'>Erreur d'execution !!!</p>
            </div>

            <!-- Example row of columns -->
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        
                        <div class="panel-heading">Espace étudiant</div>
                        <div class="panel-body">
                            <p>Consultation des cours</p>
                            <p>Consultation des absences</p>
                           <!-- <p><a class="btn btn-warning" href="${listeEtudiant}">Voir details &raquo;</a></p>-->
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Espace enseignant</div>
                        <div class="panel-body">
                            <p>Gestion des contenus des cours</p>
                            <p>Gestion des absences des étudiants</p>
                           <!-- <p><a class="btn btn-warning" href="${listeEnseignant}">Voir details &raquo;</a></p>-->
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Espace administrateur</div>
                        <div class="panel-body">
                            <p>Gestion des personnes</p>
                            <p>Gestion des matieres</p>
                            <p>Gestion des promotions</p>
                            <p>Gestion des cours</p>
                            <p>Gestion des absences</p>
                           <!-- <p><a class="btn btn-warning" href="${listeAdmin}">Voir details &raquo;</a></p>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Site footer -->
    <div class="footer">
        <p>&copy; Company 2013</p>
    </div>

</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</body> 
</html>
