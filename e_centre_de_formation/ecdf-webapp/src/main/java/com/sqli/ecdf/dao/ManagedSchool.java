package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Admin;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Enseignant;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import com.sqli.ecdf.model.exception.KeyNotThereException;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import com.sqli.ecdf.model.Promotion;
import com.sqli.ecdf.services.InterfaceAdmin;
import com.sqli.ecdf.services.EnseignantServices;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ManagedSchool implements InterfaceAdmin, EnseignantServices {

    private Map<Personne, LoginInfos> listPersonneEtLogin;
    private Map<Etudiant, String> listEtudiant;
    private Map<Admin, String> listAdmin;
    private Map<Enseignant, String> listEnseignant;
    private Map<Absence, String> listAbsence;
    private Map<Matiere, String> listMatiere;
    private Map<Promotion, String> promotion;
    private Map<Cours, String> listCours;

    public ManagedSchool() {
        listPersonneEtLogin = new HashMap<Personne, LoginInfos>();
        listEtudiant = new HashMap<Etudiant, String>();
        listAdmin = new HashMap<Admin, String>();
        listEnseignant = new HashMap<Enseignant, String>();
        listAbsence = new HashMap<Absence, String>();
        promotion = new HashMap<Promotion, String>();
        listCours = new HashMap<Cours, String>();


    }

    public Set<Etudiant> getAllEtudiants() {
        Set<Etudiant> result;
        result = new HashSet<Etudiant>();
        for (Etudiant etudiant : listEtudiant.keySet()) {
            result.add(etudiant);
        }
        return Collections.unmodifiableSet(result);

    }

    public Set<Matiere> getAllMatieres() {
        Set<Matiere> result;
        result = new HashSet<Matiere>();
        for (Matiere matiere : listMatiere.keySet()) {
            result.add(matiere);
        }
        return Collections.unmodifiableSet(result);

    }

    public Set<Absence> getAllAbsences() {
        Set<Absence> result;
        result = new HashSet<Absence>();
        for (Absence absence : listAbsence.keySet()) {
            result.add(absence);
        }
        return Collections.unmodifiableSet(result);

    }

    public Set<Admin> getAllAdmins() {
        Set<Admin> result;
        result = new HashSet<Admin>();

        for (Admin admin : listAdmin.keySet()) {
            result.add(admin);
        }

        return Collections.unmodifiableSet(result);

    }

    public Set<Enseignant> getAllEnseignants() {
        Set<Enseignant> result;
        result = new HashSet<Enseignant>();

        for (Enseignant enseignant : listEnseignant.keySet()) {
            result.add(enseignant);
        }

        return Collections.unmodifiableSet(result);

    }

    public void displayAllMaps() {

        for (Personne personne : listPersonneEtLogin.keySet()) {

            log.info("Personne {}", personne.returnType());
        }
        log.info("---------------------------------------");
        for (Personne personne : listEtudiant.keySet()) {
            log.info("Personne {}", personne.returnType());

        }
        log.info("---------------------------------------");
        for (Personne personne : listEnseignant.keySet()) {
            log.info("Personne {}", personne.returnType());

        }
        log.info("---------------------------------------");
        for (Personne personne : listAdmin.keySet()) {
            log.info("Personne {}", personne.returnType());

        }
    }

    public void addPersonneAndLogin(Personne personne, LoginInfos login) throws KeyAlreadyThereException {
        if (listPersonneEtLogin.containsKey(personne)) {
            throw new KeyAlreadyThereException("add personne and login failed !");

        }
        if (personne.returnType().equals("Etudiant")) {
            Etudiant etudiant = (Etudiant) personne;
            listEtudiant.put(etudiant, personne.getNom());
        } else if (personne.returnType().equals("Admin")) {
            Admin admin = (Admin) personne;
            listAdmin.put(admin, personne.getNom());
        } else if (personne.returnType().equals("Enseignant")) {
            Enseignant enseignant = (Enseignant) personne;
            listEnseignant.put(enseignant, personne.getNom());
        }
    }

   
    public LoginInfos getPersonneAndLogin(Personne personne) throws KeyNotThereException {
        if (!listPersonneEtLogin.containsKey(personne)) {
            throw new KeyNotThereException("personne doesn't exist !");

        }

        return listPersonneEtLogin.get(personne);
    }

    
    
 

    @Override
    public void addMatiere(String nom, Matiere matiere) {
        listMatiere.put(matiere, nom);
    }

    public void removeMatiere(Matiere matiere) {
        listMatiere.remove(matiere);
    }

 

    @Override
    public void addCours(Cours cours) {
        this.listCours.put(cours, cours.getDescription());
    }

    public void removeCours(Cours cours) {
        listCours.remove(cours);
    }

    public void addAbsence(Absence absence) {
        this.listAbsence.put(absence, absence.returnKey());

    }

    public void removeAbsence(Absence absence) {
        this.listAbsence.remove(absence);
    }

    @Override
    public void addCoursContents(Cours cours, String description) {
        cours.setDescription(description);
    }

    @Override
    public void removeCoursContents(Cours cours, String description) {
      cours.setDescription("no contents yet ");
    }


    public String displayAbsence(Absence absence) {
        return absence.getMotif();
    }

    @Override
    public void removeMatiere(String nom) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeCours(String description) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addAbsence(String motif, Cours cours, Etudiant etudiant) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeAbsence(String motif, Cours cours, Etudiant etudiant) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    @Override
    public void addPersonne(Personne personne) throws KeyAlreadyThereException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removePersonne(String keyPersonne) throws KeyNotThereException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Matiere getMatiere(String nom) throws KeyNotThereException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addPromotion(String nomPromotion, Promotion promotion) throws KeyAlreadyThereException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removePromotion(String nompromotion) throws KeyNotThereException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addAbsence(String absenceName, Absence absence) throws KeyAlreadyThereException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeAbsence(String motif) throws KeyNotThereException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Matiere> getAllExistingMatiereContentCours() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Personne> returnPersonneEtudiantDAO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Personne> returnAlonePersonneEtudiantDAO(String nom, String prenom) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Absence> getAllAbsenceEtudiantDAO() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Absence> getAllAbsenceUnEtudiantDAO(String nom, String prenom) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
