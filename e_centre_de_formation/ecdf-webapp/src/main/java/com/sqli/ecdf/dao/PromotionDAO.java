
package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Promotion;
import java.util.Map;
import java.util.Set;

public interface PromotionDAO {
  Set<Promotion>loadAllPromotion(String lsNomFichier);
  public Map<String, Promotion> getOnePromotionList(String libelle);
  public Map<String, Promotion>getAllPromotionList();
}
