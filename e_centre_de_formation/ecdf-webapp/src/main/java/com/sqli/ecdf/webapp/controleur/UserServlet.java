package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.dao.DummyUserDAO;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet(name = "UserServlet", urlPatterns = {"/Users"})
public class UserServlet extends HttpServlet {

    @Setter
    private static DummyUserDAO userService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String viewName;
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        
         if("".equals(login) || "".equals(password)){
        log.warn("login et mdp obligatoire {}",login + password.length());   
            viewName ="/WEB-INF/views/addUser.jsp";
        }
         else{
//        String encodePassword = getEncodedPassword(password);
             String lsNomFichier = getServletContext().getRealPath("/resources/login_mdp.csv");
             userService.writeFile(lsNomFichier, login + ";" + password);
             req.setAttribute("listeAllUser", userService.loadAllLogin(lsNomFichier));

             viewName = "/WEB-INF/views/listeUser.jsp";
         }
        displayJspPage(viewName, req, resp);
    }

    public void displayJspPage(String pageLocation, ServletRequest req, ServletResponse res) throws ServletException, IOException {
        RequestDispatcher disp = this.getServletContext().getRequestDispatcher(pageLocation);
        disp.forward(req, res);
    }

//    public String getEncodedPassword(String key) {
//        byte[] uniqueKey = key.getBytes();
//        byte[] hash = null;
//
//        try {
//            hash = MessageDigest.getInstance("MD5").digest(uniqueKey); //MD2, MD5, SHA-1, SHA-256, SHA-384, SHA-512
//
//        } catch (NoSuchAlgorithmException e) {
//            throw new Error("no MD5 support in this VM");
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        StringBuffer hashString = new StringBuffer();
//        for (int i = 0; i < hash.length; ++i) {
//            String hex = Integer.toHexString(hash[i]);
//            if (hex.length() == 1) {
//                hashString.append('0');
//                hashString.append(hex.charAt(hex.length() - 1));
//            } else {
//                hashString.append(hex.substring(hex.length() - 2));
//            }
//        }
//        return hashString.toString();
//    }
}