package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Admin;
import com.sqli.ecdf.model.Adresse;
import com.sqli.ecdf.model.Enseignant;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Personne;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;

public class DummyPersonneDAO implements PersonneDAO {

   private Map<String, Personne> personnes2 = new HashMap<String, Personne>();
    @Getter
    private static boolean isInitialized = false;
    
    @Override
    public Map<String, Personne> returnPersonneDAO() {

        if (!isInitialized){
            isInitialized = true;
            Personne admin1 = new Admin("RANTO", "andry", "email1", "/images/personnes/homme-img.jpg", new Adresse("rue1", 75000, "Paris"), new LoginInfos("mdpa", "andry"));
            Personne enseignant1 = new Enseignant("NICHELE", "Sebastien", "email2", "/images/personnes/homme-img.jpg", new Adresse("35 Av de la Resistane", 60200, "Chantilly"), new LoginInfos("mdpseb", "sebastien"));
            Personne etudiant1 = new Etudiant("KANDATE", "Gervil", "email3", "/images/personnes/homme-img.jpg", new Adresse("105 Av de Java", 74500, "Oracle"), new LoginInfos("mdpg", "gervil"), new Date(2013 / 12 / 05));
            Personne etudiant2 = new Etudiant("NGOKIO", "Christian", "email4", "/images/personnes/homme-img.jpg", new Adresse("3 Rue de Lille", 93400, "Saint Ouen"), new LoginInfos("mdpcri", "christian"), new Date(2013 / 12 / 05));
            Personne etudiant3 = new Etudiant("JUNKER", "Nicolas", "email5", "/images/personnes/homme-img.jpg", new Adresse("256 rue de Paris", 92600, "Boulogne"), new LoginInfos("mdpnic", "nicolas"), new Date(2013 / 12 / 05));
            Personne admin2 = new Admin("LANGLAIS", "Sophie", "email6", "/images/personnes/femme-img.jpg", new Adresse("rue1", 75000, "Paris"), new LoginInfos("mdpsoph", "sophie"));

            personnes2.put(admin1.returnKey(), admin1);
            personnes2.put(enseignant1.returnKey(), enseignant1);
            personnes2.put(etudiant1.returnKey(), etudiant1);
            personnes2.put(etudiant2.returnKey(), etudiant2);
            personnes2.put(etudiant3.returnKey(), etudiant3);
            personnes2.put(admin2.returnKey(), admin2);
        }

        return personnes2;
    }
    
    public void addPersonne(Personne pers){
        personnes2.put(pers.returnKey(), pers);
    }
    
    public Personne getPersonne(String nom, String prenom, String role){
        Personne result = personnes2.get(nom + "_" + prenom + "_" + role);
        return result;
    }
    
    
    public void removePersonne(Personne pers){
        personnes2.remove(pers.returnKey());
    }

    //-- pour afficher une personne dans la fiche
    @Override
    public Map<String, Personne> returnAlonePersonneDAO(String nom, String prenom) {

        Map<String, Personne> personnes = returnPersonneDAO();
        Map<String, Personne> alonePersonne = new HashMap<String, Personne>();

        for (Personne personne : personnes.values()) {
            if (personne.getNom().equals(nom) && personne.getPrenom().equals(prenom)) {
                alonePersonne.put(personne.returnKey(), personne);
                break;
            }
        }

        if (personnes.size() == 0) {
            Personne pers = new Admin("", "", "", "", new Adresse("", 0, ""), new LoginInfos("", ""));
            alonePersonne.put(pers.returnKey(), pers);
        }

        return alonePersonne;

    }

    //-- pour la liste des personnes connectées
    @Override
    public String returnAloneUserPersonneDAO(String loginUser) {

        Map<String, Personne> personnes = returnPersonneDAO();
        Map<String, Personne> personneConnecter = new HashMap<>();
        String role = "";


        for (Personne personne : personnes.values()) {
            if (personne.getLoginInfos().getLogin().equals(loginUser)) {
                personneConnecter.put(personne.returnKey(), personne);
                role = personne.returnType();
                break;
            }
        }
        return role;
    }

    //-- cherche l'information de login et password dans la dao du LoginInfos
    @Override
    public boolean isIdentification(String loginForm, String passwordForm) {

        // on récupère la liste dans la base de donnée (ici en dur)
        Map<String, Personne> personnes = returnPersonneDAO();

        for (Personne personne : personnes.values()) {
            if (personne.getLoginInfos().getMotDePasse().equals(passwordForm) && personne.getLoginInfos().getLogin().equals(loginForm)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Map<String, Personne> returnPersonneEtudiantDAO() {
        Map<String, Personne> personnes = returnPersonneDAO();
        Map<String, Personne> personneEtudiant = new HashMap<>();

        for (Personne personne : personnes.values()) {
            if (personne.returnType().equals("Etudiant")) {
                personneEtudiant.put(personne.returnKey(), personne);
            }
        }
        return personneEtudiant;
    }

    @Override
    public Map<String, Personne> returnOnePersonneEtudiantDAO(String loginUser) {
        Map<String, Personne> personnes = returnPersonneEtudiantDAO();
        Map<String, Personne> personneOneEtudiant = new HashMap<>();

        for (Personne personne : personnes.values()) {
            if (personne.getLoginInfos().getLogin().equals(loginUser)) {
                personneOneEtudiant.put(personne.returnKey(), personne);
            }
        }
        return personneOneEtudiant;
    }

    @Override
    public Map<String, Personne> returnAlonePersonneEtudiantDAO(String nom, String prenom) {
        Map<String, Personne> personnes = returnPersonneEtudiantDAO();
        Map<String, Personne> alonePersonneEtudiant = new HashMap<String, Personne>();

        for (Personne personne : personnes.values()) {
            if (personne.getNom().equals(nom) && personne.getPrenom().equals(prenom)) {
                alonePersonneEtudiant.put(personne.returnKey(), personne);
                break;
            }
        }
        if (personnes.size() == 0) {
            Personne pers = new Admin("", "", "", "", new Adresse("", 0, ""), new LoginInfos("", ""));
            alonePersonneEtudiant.put(pers.returnKey(), pers);
        }
        return alonePersonneEtudiant;
    }
}
