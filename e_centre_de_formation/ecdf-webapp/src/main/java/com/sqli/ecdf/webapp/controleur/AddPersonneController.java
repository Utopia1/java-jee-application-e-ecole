/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.dao.DummyPersonneDAO;
import com.sqli.ecdf.model.Admin;
import com.sqli.ecdf.model.Adresse;
import com.sqli.ecdf.model.Enseignant;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Personne;
import com.sqli.ecdf.services.DummyAdminServices;
import static com.sqli.ecdf.webapp.controleur.AddPersonneServlet.adminServices;
import static com.sqli.ecdf.webapp.controleur.AddPersonneServlet.serviceDummyPersonneDAO;
import java.sql.Date;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author stagiaire
 */
@Slf4j
@Controller
public class AddPersonneController {
    
    @Setter
    @Autowired
    public DummyPersonneDAO serviceDummyPersonneDAO;
    @Setter
    @Autowired
    public DummyAdminServices adminServices;
    
    @RequestMapping(value = "/AddPersonneServlet", method = RequestMethod.POST)
    public ModelAndView performAddPersonneController(
        @RequestParam(value = "nom", required = false) String nom,
        @RequestParam(value = "prenom", required = false) String prenom,
        @RequestParam(value = "dateDeNaissance", required = false) String dateDeNaissance,
        @RequestParam(value = "adresse", required = false) String adresse,
        @RequestParam(value = "codePostal", required = false) int codePostal,
        @RequestParam(value = "ville", required = false) String ville,
        @RequestParam(value = "email", required = false) String email,
        @RequestParam(value = "utilisateur", required = false) String utilisateur,
        @RequestParam(value = "password", required = false) String password,
        @RequestParam(value = "roleSelect", required = false)String role){
        
        
        String dummyPhoto = "dummyPhoto";
        ModelAndView mv = new ModelAndView();
        
        Personne personne =new Admin("nom4","prenom4","eMail1","photo1",new Adresse("rue1",12345,"lyon"),new LoginInfos("mdp1","login2"));
        
        if("Admin".equals(role)){
            personne = new Admin(nom, prenom, email, dummyPhoto, new Adresse(adresse, Integer.valueOf(codePostal), ville), new LoginInfos(password, utilisateur));
        }
        else if ("Etudiant".equals(role)){
            Date date = Date.valueOf("dateDeNaissance");
            personne = new Etudiant(nom, prenom, email, dummyPhoto, new Adresse(adresse, Integer.valueOf(codePostal), ville), new LoginInfos(password, utilisateur), date);
        }
        else if ("Ensignant".equals(role)){
            personne = new Enseignant(nom, prenom, email, dummyPhoto, new Adresse(adresse, Integer.valueOf(codePostal), ville), new LoginInfos(password, utilisateur));
        }
        
        log.info("Peronne créee {}", personne.toString());
        
        mv.addObject("listeDePersonne", adminServices.returnPersonneDAO());
        mv.setViewName("listePersonne");
        
        serviceDummyPersonneDAO.addPersonne(personne);
        
        return mv;
    
    }
    
}
