package com.sqli.ecdf.services;

import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Etudiant;

public interface InterfaceEnseignant {

    void addCoursContents(Cours cours, String description);

    void removeCoursContents(Cours cours, String description);

    void addAbsence(String motif, Cours cours, Etudiant etudiant);

    void removeAbsence(String motif, Cours cours, Etudiant etudiant);
}
