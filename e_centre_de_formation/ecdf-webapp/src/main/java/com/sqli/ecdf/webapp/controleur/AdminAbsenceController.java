/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.dao.DummyAbsenceDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author stagiaire
 */
@Controller
@Slf4j
public class AdminAbsenceController {
    
    @Autowired
    private DummyAbsenceDAO absencesService;
    
    @RequestMapping("/ListAbsencesServlet")
    public ModelAndView adminAbsences(){
        ModelAndView mv = new ModelAndView();
        
        mv.addObject("listeDesAbsences", absencesService.getAllAbsences());
        mv.setViewName("listeAbsence");
        
        
        return mv;
    }
}
