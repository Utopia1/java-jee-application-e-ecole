package com.sqli.ecdf.services;

import com.sqli.ecdf.dao.AbsenceDAO;
import com.sqli.ecdf.dao.CoursContentsDAO;
import com.sqli.ecdf.dao.PersonneDAO;
import com.sqli.ecdf.dao.PromotionDAO;
import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import com.sqli.ecdf.model.Promotion;
import java.util.List;
import java.util.Map;
import lombok.Setter;

public class DummyAdminServices implements AdminsServices{
    @Setter
    private CoursContentsDAO coursContentsDAO;
    @Setter
    private PersonneDAO personneDAO;
    @Setter
    private AbsenceDAO absenceEtudiantDAO;
    
    private PromotionDAO promotionDAO;
    
    @Override
    public Map<String, Matiere> getOneMatiereListCours(String nom){
        return coursContentsDAO.getOneMatiereListCours(nom);
    }
    
    @Override
    public Map<String, Personne> returnPersonneDAO(){
        return personneDAO.returnPersonneDAO();
    }

    @Override
    public Map<String, Personne> returnPersonneEtudiantDAO(){
        return personneDAO.returnPersonneEtudiantDAO();
    }

    @Override
    public Map<String, Personne> returnAlonePersonneEtudiantDAO(String nom, String prenom){
        return personneDAO.returnAlonePersonneEtudiantDAO(nom, prenom);
    }

    @Override
    public Map<String, Absence> getAllAbsenceEtudiantDAO(){
        return absenceEtudiantDAO.getAllAbsenceEtudiantDAO();
    }

    @Override
    public Map<String, Absence> getAllAbsenceUnEtudiantDAO(String nom, String prenom){
        return absenceEtudiantDAO.getAllAbsenceUnEtudiantDAO(nom, prenom);
    }

    @Override
    public List<Matiere> getAllExistingMatiereContentCours() {
        return coursContentsDAO.getAllExistingMatiereContentCours();
    }

    @Override
    public Map<String, Personne> returnAlonePersonneDAO(String nom, String prenom) {
        return personneDAO.returnAlonePersonneDAO(nom, prenom);
    }
    
    @Override
    public Map<String, Promotion>getAllPromotionList(){
        return promotionDAO.getAllPromotionList();
    }
    
    @Override
    public Map<String, Promotion> getOnePromotionList(String libelle){
        return promotionDAO.getOnePromotionList(libelle);
    }
    
}
