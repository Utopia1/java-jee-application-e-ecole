package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.LoginInfos;
import java.util.List;
import java.util.Set;

public interface UserDAO {

    Set<LoginInfos> loadAllLogin(String lsNomFichier);

    public boolean isIdentifier(String loginForm, String passwordForm);

    public List<LoginInfos> userList();
}
