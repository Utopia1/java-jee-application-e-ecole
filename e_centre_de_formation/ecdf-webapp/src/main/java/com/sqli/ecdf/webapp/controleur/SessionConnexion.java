package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.dao.DummyPersonneDAO;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.services.DummyAdminServices;
import com.sqli.ecdf.services.DummyConnectionService;
import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@WebServlet(name = "SessionConnexion", urlPatterns = {"/Connexion"})
public class SessionConnexion extends HttpServlet {

    @Setter
    private static DummyPersonneDAO personneService;
    @Setter
    private static DummyConnectionService connectionService;
    @Setter
    private static DummyAdminServices adminService;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String viewName;
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        HttpSession session = req.getSession();
        String role;
        boolean userIdentification = isIdentifier(login, password);

        if (userIdentification == false) {
            log.warn("Connection failed ! {}, {}", login, userIdentification);
            viewName = "/index.jsp";
        } else {
            String userConnected = req.getParameter("login");
            role = personneService.returnAloneUserPersonneDAO(userConnected);
            log.info("resultat et login {}", role + login);
            req.setAttribute("listeDePersonne", adminService.returnPersonneDAO());
            req.setAttribute("listeEtudiant", adminService.returnPersonneEtudiantDAO());
            req.setAttribute("listeMatiere", adminService.getAllExistingMatiereContentCours());
            req.setAttribute("listeAbsence", adminService.getAllAbsenceEtudiantDAO());
            session.setAttribute("user", login);

            if ("Admin".equals(role)) {
                log.info("resultat et login {}", userIdentification + login);
                viewName = "/WEB-INF/views/admin.jsp";
                req.setAttribute("listeDePersonne", personneService.returnAlonePersonneDAO(login, password));
            } else {
                if ("Enseignant".equals(role)) {
                    log.info("resultat et login {}", userIdentification + login);
                    viewName = "/WEB-INF/views/enseignant.jsp";
                } else {
                    if ("Etudiant".equals(role)) {
                        req.setAttribute("infoEtudiant", personneService.returnOnePersonneEtudiantDAO(userConnected));
                        viewName = "/WEB-INF/views/etudiant.jsp";
                    } else {
                        viewName = "/index.jsp";
                        req.getSession().invalidate();
                    }

                }
            }
        }
        displayJspPage(viewName, req, resp);
    }

    public void displayJspPage(String pageLocation, ServletRequest req, ServletResponse res) throws ServletException, IOException {
        RequestDispatcher disp = this.getServletContext().getRequestDispatcher(pageLocation);
        disp.forward(req, res);
    }

    public boolean isIdentifier(String loginForm, String passwordForm) {
        // on récupère la liste dans la base de donnée (ici en dur)
        List<LoginInfos> listUser = connectionService.userList();
        //on parcour la liste et on teste les entrées du formulaire.
        for (int i = 0; i < listUser.size(); i++) {
            if (passwordForm.equalsIgnoreCase(listUser.get(i).getMotDePasse()) & loginForm.equalsIgnoreCase(listUser.get(i).getLogin())) {
                return true;
            }
        }
        return false;
    }
}
