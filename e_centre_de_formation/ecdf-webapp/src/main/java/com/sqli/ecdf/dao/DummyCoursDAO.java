package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Cours;
import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DummyCoursDAO implements CoursDAO {

    
    @Override
    public Set<Cours> loadAllCours(String lsNomFichier) {
        StringBuilder lsbContenu = new StringBuilder();
        Set<Cours> tLignesCours = new HashSet<>();
        ArrayList tAllLignes = new ArrayList();

        try {
//            lsNomFichier = "/resources/promotions.csv";
            // --- Lecture par ligne
            FileReader lfrFichier;
            BufferedReader lbrBuffer;
            String lsLigne;
            String[] tChamps;


            lfrFichier = new FileReader(lsNomFichier);
            lbrBuffer = new BufferedReader(lfrFichier);
            while ((lsLigne = lbrBuffer.readLine()) != null) {
                tAllLignes.add(lsLigne);
            }
            for (int i = 1; i < tAllLignes.size(); i++) {
                tChamps = tAllLignes.get(i).toString().split(";");
                DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                Date date = df.parse(tChamps[0]);


                Cours cours = new Cours(date, Integer.parseInt(tChamps[1]), tChamps[2]);
                tLignesCours.add(cours);
            }

            lbrBuffer.close();
            lfrFichier.close();
        } catch (Exception err) {
            lsbContenu.append(err.getMessage());
        }
        return tLignesCours;
    }

    public static Date parseDate(String date) throws ParseException {
        Date result = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat();
            result = formatter.parse(date);
            return result;
        } catch (RuntimeException e) {
            return null;
        }
    }

   
    @Override
    public Set<Cours> loadAgainAllCours() {
        Set<Cours> coursToReturn = new HashSet<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date1 = df.parse("20/12/2012");
            Date date2 = df.parse("01/08/2012");
            Date date3 = df.parse("20/09/2012");
            Date date4 = df.parse("15/12/2012");
            Date date5 = df.parse("05/02/2013");
            
        coursToReturn.addAll(Arrays.asList(
                new Cours(date1, 420, "JAVA"),
                new Cours(date2, 380, "HTML"),
                new Cours(date3, 380, "COMMUNICATION"),
                new Cours(date4, 210, "ANDROID"),
                new Cours(date5, 80, "JAVASCRIPT")));

        }
        catch (ParseException ex) {
            Logger.getLogger(DummyCoursDAO.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return coursToReturn;
    }
    
    @Override
    public List<Cours>getAllExistingCours(){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        List<Cours>listCours = new ArrayList();
         try {
            Date date1 = df.parse("20/12/2012");
            Date date2 = df.parse("01/08/2012");
            Date date3 = df.parse("20/09/2012");
            Date date4 = df.parse("15/12/2012");
            Date date5 = df.parse("05/02/2013");
        listCours.addAll(Arrays.asList(
                new Cours(date1, 420, "JAVA"),
                new Cours(date2, 380, "HTML"),
                new Cours(date3, 380, "COMMUNICATION"),
                new Cours(date4, 210, "ANDROID"),
                new Cours(date5, 80, "JAVASCRIPT")));
        }
        catch (ParseException ex) {
            Logger.getLogger(DummyCoursDAO.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return listCours;
    }
}
