/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.dao.AdminDAO;
import com.sqli.ecdf.dao.DummyAbsenceDAO;
import com.sqli.ecdf.model.Absence;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author stagiaire
 */
@Slf4j
@WebServlet(name = "AdminAbsenceServlet", urlPatterns = {"/ListAbsencesServlet"})
public class AdminAbsenceServlet extends HttpServlet {

    @Setter
    private static DummyAbsenceDAO absencesService;
    
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String viewName = "/WEB-INF/views/listeAbsence.jsp";
        
        req.setAttribute("listeDesAbsences", absencesService.getAllAbsences());
        log.info("***********************");
        log.info("nombre d'absences {}",absencesService.getAllAbsences().size() );
        log.info("***********************");
        displayJspPage(viewName, req, resp);
    }

    
    public void displayJspPage(String pageLocation, ServletRequest req, ServletResponse res) throws ServletException, IOException {
        RequestDispatcher disp = this.getServletContext().getRequestDispatcher(pageLocation);
        disp.forward(req, res);

    }
}
