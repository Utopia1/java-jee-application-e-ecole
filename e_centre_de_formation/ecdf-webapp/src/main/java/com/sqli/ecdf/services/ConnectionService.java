package com.sqli.ecdf.services;

import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Personne;
import java.util.List;
import java.util.Map;

public interface ConnectionService {
public boolean isIdentifier(String loginForm, String passwordForm); 
 public List<LoginInfos> userList();
 public Map<String, Personne> returnOnePersonneEtudiantDAO(String loginUser);
}
