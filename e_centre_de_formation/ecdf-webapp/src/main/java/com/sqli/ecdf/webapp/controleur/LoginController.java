/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.services.ConnectionService;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author stagiaire
 */
@Slf4j
@Controller
public class LoginController {

    @Setter
    @Autowired
    private ConnectionService authentificationService;

    @RequestMapping("/LoginServlet")
    public ModelAndView login() {

        ModelAndView mv = new ModelAndView();



        return mv;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView login(
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "login", required = false) String login,
            HttpServletRequest request) throws IOException, ServletException {

        log.error("LoginServlet has received login = {} and [SECRET] password", login);

        return processLoginAndPasword(login, password, request);
    }

    private boolean checkThatFormHasBeenCorrectlySubmitted(String login, String password) {
        return login != null && password != null;
    }

    private boolean checkThatAllFieldsHaveBeenFilled(String login, String password) {
        return !login.isEmpty() && !password.isEmpty();
    }

    private ModelAndView processLoginAndPasword(
            String login,
            String password,
            HttpServletRequest req) throws IOException, ServletException {

        ModelAndView mv = new ModelAndView();


        // 2 , Traitements
        if (checkThatFormHasBeenCorrectlySubmitted(login, password)) {
            if (checkThatAllFieldsHaveBeenFilled(login, password)) {
                try {
                    boolean u = authentificationService.isIdentifier(login, password);
                    req.getSession().setAttribute("user", u);
                    mv.setViewName("redirect:/admin");
                } catch (Exception ex) {
                    log.warn("Invalid credentials submitted for login {} and a password of length {}", login, password.length());
                    mv.addObject("ERROR_MESSAGE", ex.getMessage());
                }
            } else {
                log.info("Somebody is submitting an empty form ?");
                String message = "Merci de remplir le formulaire complètement !";
                mv.addObject("ERROR_MESSAGE", message);

            }
        } else {
            log.warn("Login form has been submitted incorrectly with some null field !");
            String message = "Appel incorrect !";

            mv.addObject("ERROR_MESSAGE", message);
        }
        return mv;
    }
    // 3 , Affichage de la page
}
