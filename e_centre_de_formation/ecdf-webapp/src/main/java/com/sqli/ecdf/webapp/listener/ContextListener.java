package com.sqli.ecdf.webapp.listener;

import com.sqli.ecdf.dao.DummyAbsenceDAO;
import com.sqli.ecdf.dao.DummyCoursContentsDAO;
import com.sqli.ecdf.dao.DummyCoursDAO;
import com.sqli.ecdf.dao.DummyPersonneDAO;
import com.sqli.ecdf.dao.DummyPromotionDAO;
import com.sqli.ecdf.dao.DummyUserDAO;
import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import com.sqli.ecdf.services.DummyAdminServices;
import com.sqli.ecdf.services.DummyConnectionService;
import com.sqli.ecdf.services.DummyEnseignantServices;
import com.sqli.ecdf.services.DummyEtudiantServices;
import com.sqli.ecdf.webapp.controleur.AddPersonneServlet;
import com.sqli.ecdf.webapp.controleur.AdminServlet;
import com.sqli.ecdf.webapp.controleur.CrudPersonneServlet;
import com.sqli.ecdf.webapp.controleur.EnseignantServlet;
import com.sqli.ecdf.webapp.controleur.EtudiantServlet;
import com.sqli.ecdf.webapp.controleur.AdminAbsenceServlet;
import com.sqli.ecdf.webapp.controleur.SessionConnexion;
import com.sqli.ecdf.webapp.controleur.UserServlet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import lombok.extern.slf4j.Slf4j;

@WebListener()
@Slf4j
public class ContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("ECDF Web application starting ....");
        try {
            performApplicationConstructionByInjectionDependencies();
        } catch (KeyAlreadyThereException ex) {
            Logger.getLogger(ContextListener.class.getName()).log(Level.SEVERE, null, ex);
        }
        log.info("ECDF chargement application");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("ECDF Web application destroyed...");
    }

    private void performApplicationConstructionByInjectionDependencies() throws KeyAlreadyThereException {
        
//        AdminDAO personneDAO = new AdminDAO();
        DummyPromotionDAO promotionDAO = new DummyPromotionDAO();
        DummyCoursDAO coursDAO = new DummyCoursDAO();
        DummyUserDAO userDAO = new DummyUserDAO();
        DummyPersonneDAO personneDAO = new DummyPersonneDAO();
        DummyAbsenceDAO absenceDAO = new DummyAbsenceDAO();
        DummyCoursContentsDAO coursContentsDAO = new DummyCoursContentsDAO();
        

        UserServlet.setUserService(userDAO);
        SessionConnexion.setPersonneService(personneDAO);
        
        AddPersonneServlet.setServiceDummyPersonneDAO(personneDAO);
        AdminServlet.setPersonneService(personneDAO);
        CrudPersonneServlet.setPersonneService(personneDAO);
        AdminServlet.setPromotionService(promotionDAO);
        AdminServlet.setCoursService(coursDAO);
        AdminServlet.setUserService(userDAO);
        AdminAbsenceServlet.setAbsencesService(absenceDAO);
        
        
        DummyAdminServices adminServices = new DummyAdminServices();
        adminServices.setPersonneDAO(personneDAO);
        adminServices.setCoursContentsDAO(coursContentsDAO);
        adminServices.setAbsenceEtudiantDAO(absenceDAO);
        AdminServlet.setAdminServices(adminServices);

        DummyEnseignantServices enseignantServices = new DummyEnseignantServices();
        enseignantServices.setPersonneEtudiantDAO(personneDAO);
        enseignantServices.setCoursContentsDAO(coursContentsDAO);
        enseignantServices.setAbsenceEtudiantDAO(absenceDAO);
        EnseignantServlet.setCoursService(coursDAO);
        EnseignantServlet.setEnseignantServices(enseignantServices);
        

        DummyConnectionService connectionService = new DummyConnectionService();
        connectionService.setUserDAO(userDAO);
        connectionService.setPersonneDAO(personneDAO);
        SessionConnexion.setConnectionService(connectionService);
        SessionConnexion.setAdminService(adminServices);
        
        DummyEtudiantServices etudiantServices = new DummyEtudiantServices();
        etudiantServices.setPersonneDAO(personneDAO);
        etudiantServices.setCoursContentsDAO(coursContentsDAO);
        etudiantServices.setCoursDAO(coursDAO);
        etudiantServices.setAbsenceDAO(absenceDAO);
        EtudiantServlet.setServiceEtudiant(etudiantServices);
         
//        EtudiantDAO matiereCours = new EtudiantDAO(personneDAO);
//        EtudiantServlet.setServiceEtudiant(matiereCours);
        
        
    }
}
