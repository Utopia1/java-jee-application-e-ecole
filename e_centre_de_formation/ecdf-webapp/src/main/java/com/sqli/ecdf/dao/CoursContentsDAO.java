package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Matiere;
import java.util.List;
import java.util.Map;

public interface CoursContentsDAO {

    public List<Matiere> getAllExistingMatiereContentCours();
    public Map<String, Matiere> getAllMatiereListCours();
    public Map<String, Matiere> getOneMatiereListCours(String nomMatiere);
}
