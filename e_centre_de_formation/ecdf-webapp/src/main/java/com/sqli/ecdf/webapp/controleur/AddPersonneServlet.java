/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.webapp.controleur;

import com.sqli.ecdf.dao.DummyPersonneDAO;
import com.sqli.ecdf.model.Admin;
import com.sqli.ecdf.model.Adresse;
import com.sqli.ecdf.model.Enseignant;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Personne;
import com.sqli.ecdf.services.DummyAdminServices;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author stagiaire
 */

@Slf4j
@WebServlet(name = "AddPersonneServlet", urlPatterns = {"/AddPersonneServlet"})
public class AddPersonneServlet extends HttpServlet {
    
    @Setter
    public static DummyPersonneDAO serviceDummyPersonneDAO;
    @Setter
    public static DummyAdminServices adminServices;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        boolean isAdd;
        log.info("************************************");
        String nom = req.getParameter("nom");
        log.info("nom {}", nom);
        String prenom = req.getParameter("prenom");
        log.info("prenom {}", prenom);
        String dateDeNaissance = req.getParameter("dateDeNaissance");
        Date date = new Date(2013/10/05);
        log.info("dateDeNaissance {}", date.toString());
        String adresse = req.getParameter("adresse");
        log.info("adresse {}", adresse);
        String codePostal = req.getParameter("codePostal");
        log.info("codePostal {} ", codePostal);
        String ville = req.getParameter("ville");
        log.info("ville {}", ville);
        String eMail = req.getParameter("email");
        log.info("email {}", eMail);
        String photo = "photo";
        String login = req.getParameter("utilisateur");
        log.info("login {}", login);
        String password = req.getParameter("password");
        log.info("password", password);
        String role = req.getParameter("roleSelect");
        log.info("role {}", role);
        
        Personne personne =new Admin("nom4","prenom4","eMail1","photo1",new Adresse("rue1",12345,"lyon"),new LoginInfos("mdp1","login2"));
        
        if("Admin".equals(role)){
            personne = new Admin(nom, prenom, eMail, photo, new Adresse(adresse, Integer.valueOf(codePostal), ville), new LoginInfos(password, login));
        }
        else if ("Etudiant".equals(role)){
            personne = new Etudiant(nom, prenom, eMail, photo, new Adresse(adresse, Integer.valueOf(codePostal), ville), new LoginInfos(password, login), date);
        }
        else if ("Ensignant".equals(role)){
            personne = new Enseignant(nom, prenom, eMail, photo, new Adresse(adresse, Integer.valueOf(codePostal), ville), new LoginInfos(password, login));
        }
        
        log.info("Peronne créee {}", personne.toString());
        
        String viewName;
        
        serviceDummyPersonneDAO.addPersonne(personne);
        req.setAttribute("listeDePersonne", adminServices.returnPersonneDAO());
        viewName = "/WEB-INF/views/listePersonne.jsp";
        displayJspPage(viewName, req, resp);
    }

    

    public void displayJspPage(String pageLocation, ServletRequest req, ServletResponse res) throws ServletException, IOException {
        RequestDispatcher disp = this.getServletContext().getRequestDispatcher(pageLocation);
        disp.forward(req, res);

    }
    
}
