package com.sqli.ecdf.services;

import com.sqli.ecdf.dao.AbsenceDAO;
import com.sqli.ecdf.dao.CoursContentsDAO;
import com.sqli.ecdf.dao.PersonneDAO;
import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import java.util.List;
import java.util.Map;
import lombok.Setter;

public class DummyEnseignantServices implements EnseignantServices {

    @Setter
    private CoursContentsDAO coursContentsDAO;
    @Setter
    private PersonneDAO personneEtudiantDAO;
    @Setter
    private AbsenceDAO absenceEtudiantDAO;

    @Override
    public void addCoursContents(Cours cours, String description) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeCoursContents(Cours cours, String description) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addAbsence(String motif, Cours cours, Etudiant etudiant) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeAbsence(String motif, Cours cours, Etudiant etudiant) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Matiere> getAllExistingMatiereContentCours() {
        return coursContentsDAO.getAllExistingMatiereContentCours();
    }
    
    @Override
    public Map<String, Personne> returnPersonneEtudiantDAO(){
        return personneEtudiantDAO.returnPersonneEtudiantDAO();
    }
    
    @Override
    public Map<String, Personne> returnAlonePersonneEtudiantDAO(String nom, String prenom){
        return personneEtudiantDAO.returnAlonePersonneEtudiantDAO(nom, prenom);
    }
    
    @Override
    public Map<String, Absence> getAllAbsenceEtudiantDAO(){
        return absenceEtudiantDAO.getAllAbsenceEtudiantDAO();
    }
    
    @Override
     public Map<String, Absence> getAllAbsenceUnEtudiantDAO(String nom, String prenom){
         return absenceEtudiantDAO.getAllAbsenceUnEtudiantDAO(nom, prenom);
     }
    
}
