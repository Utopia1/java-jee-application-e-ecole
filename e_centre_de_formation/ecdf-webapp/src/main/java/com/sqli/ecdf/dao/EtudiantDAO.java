package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Adresse;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import com.sqli.ecdf.model.LoginInfos;
import com.sqli.ecdf.model.Matiere;
import com.sqli.ecdf.model.Personne;
import com.sqli.ecdf.services.AdminServices;
import java.util.Date;
import java.util.Set;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EtudiantDAO extends AdminServices{

    @Setter
    private AdminServices etudiantDAO;
    
    public EtudiantDAO(AdminServices etudiantDAO) throws KeyAlreadyThereException {
        this.etudiantDAO = etudiantDAO;
        init();
    }

    public void init() throws KeyAlreadyThereException {
        
        Personne personne = new Personne("nom1", "prenom1", "email1", "photo1", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp2", "login2"));
        Personne etudiant2 = new Etudiant("nom2", "prenom2", "email2", "photo2", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp2", "login2"), new Date(2013 / 12 / 05));
        Personne etudiant3 = new Etudiant("nom3", "prenom3", "email2", "photo2", new Adresse("rue2", 05600, "Saint Firmin"), new LoginInfos("mdp3", "login23"), new Date(2013 / 12 / 05));
        log.info("*********************before personnes {}", personne.getNom());
        etudiantDAO.addPersonne(personne);
        log.info("*********************before One personnes {}", personne.getNom());
        
        etudiantDAO.addPersonne(etudiant2);
        etudiantDAO.addPersonne(etudiant3);
        log.info("**********************after personnes {}", personne.getPrenom());
        Cours cours1 = new Cours(new Date(2012 / 05 / 05), 420, "JAVA");
        Cours cours2 = new Cours(new Date(2012 / 07 / 06), 380, "HTML");
        Cours cours3 = new Cours(new Date(2012 / 01 / 04), 380, "COMMUNICATION");
        log.info("before cours {}", cours1.getDescription());
        etudiantDAO.addCours(cours1);
        etudiantDAO.addCours(cours2);
        etudiantDAO.addCours(cours3);
        
        Matiere matiere1 = new Matiere("Introduction JAVA");
        Matiere matiere2 = new Matiere("Maven");
        Matiere matiere3 = new Matiere("Javascript");
        etudiantDAO.addMatiere("JAVA", matiere1);
        etudiantDAO.addMatiere("Maquette", matiere2);
        etudiantDAO.addMatiere("CV", matiere3);
        
        matiere1.addCours(cours1);
        matiere2.addCours(cours2);
        matiere3.addCours(cours3);
        
        
    }

    public  Set<Matiere>displayAllMatieres() {
        
        Set<Matiere> matieres = etudiantDAO.getAllMatieres();
        
        for (Matiere matiere : matieres) {
            log.info("matiere {}", matiere.getNom());
            for (Cours cour : matiere.getAllCours()) {
                log.info("cours {}", cour.getDescription());
            }
        }        
       return matieres; 
    }    
}
