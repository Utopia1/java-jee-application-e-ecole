package com.sqli.ecdf.dao;

import com.sqli.ecdf.model.Absence;
import com.sqli.ecdf.model.Adresse;
import com.sqli.ecdf.model.Cours;
import com.sqli.ecdf.model.Etudiant;
import com.sqli.ecdf.model.LoginInfos;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DummyAbsenceDAO implements AbsenceDAO {

    public static Date parseDate(String date) throws ParseException {
        Date result = null;
        try {
            SimpleDateFormat formatter = new SimpleDateFormat();
            result = formatter.parse(date);
            return result;
        } catch (RuntimeException e) {
            return null;
        }
    }

    @Override
    public Set<Absence> getAllAbsences() {
        Set<Absence> absenceToReturn = new HashSet<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date1 = df.parse("20/12/2012");
            Etudiant etudiant1 = new Etudiant("KANDATE", "Gervil", "email3", "/images/personnes/homme-img.jpg", new Adresse("105 Av de Java", 74500, "Oracle"), new LoginInfos("mdp2", "login2"), new Date(2013 / 12 / 05));
            Date date2 = df.parse("20/01/2013");
            Etudiant etudiant2 = new Etudiant("JUNKER", "Nicolas", "email5", "/images/personnes/homme-img.jpg", new Adresse("256 rue de Paris", 92600, "Boulogne"), new LoginInfos("mdp2", "login2"), new Date(2013 / 12 / 05));
            absenceToReturn.addAll(Arrays.asList(
                    new Absence("maladie", new Cours(date1, 420, "JAVA"), etudiant1),
                    new Absence("maladie", new Cours(date2, 420, "JAVA"), etudiant2)));
        } catch (ParseException ex) {
            Logger.getLogger(DummyCoursDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return absenceToReturn;
    }

    @Override
    public List<Absence> getAllExistingAbsence() {
        List<Absence> listAbsence = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date1 = df.parse("20/12/2012");
            Etudiant etudiant1 = new Etudiant("KANDATE", "Gervil", "email3", "/images/personnes/homme-img.jpg", new Adresse("105 Av de Java", 74500, "Oracle"), new LoginInfos("mdp2", "login2"), new Date(2013 / 12 / 05));
            Date date2 = df.parse("20/01/2013");
            Etudiant etudiant2 = new Etudiant("JUNKER", "Nicolas", "email5", "/images/personnes/homme-img.jpg", new Adresse("256 rue de Paris", 92600, "Boulogne"), new LoginInfos("mdp2", "login2"), new Date(2013 / 12 / 05));

            listAbsence.addAll(Arrays.asList(
                    new Absence("maladie", new Cours(date1, 420, "JAVA"), etudiant1),
                    new Absence("maladie", new Cours(date2, 420, "JAVA"), etudiant2)));
        } catch (ParseException ex) {
            Logger.getLogger(DummyAbsenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listAbsence;

    }

    @Override
    public Map<String, Absence> getAllAbsenceEtudiantDAO() {
        Map<String, Absence> absences = new HashMap<String, Absence>();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date1 = df.parse("20/12/2012");
            Etudiant etudiant1 = new Etudiant("KANDATE", "Gervil", "email3", "/images/personnes/homme-img.jpg", new Adresse("105 Av de Java", 74500, "Oracle"), new LoginInfos("mdp2", "login2"), new Date(2013 / 12 / 05));
            Date date2 = df.parse("20/01/2013");
            Etudiant etudiant2 = new Etudiant("JUNKER", "Nicolas", "email5", "/images/personnes/homme-img.jpg", new Adresse("256 rue de Paris", 92600, "Boulogne"), new LoginInfos("mdp2", "login2"), new Date(2013 / 12 / 05));
           
            absences.put(etudiant1.returnKey(), new Absence("maladie", new Cours(date1, 420, "JAVA"), etudiant1));
            absences.put(etudiant2.returnKey(), new Absence("personnelle", new Cours(date2, 420, "JAVA"), etudiant2));
        } catch (ParseException ex) {
            Logger.getLogger(DummyAbsenceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return absences;
    }
    
    @Override
     public Map<String, Absence> getAllAbsenceUnEtudiantDAO(String nom, String prenom) {

        Map<String, Absence> absences = getAllAbsenceEtudiantDAO();
        Map<String, Absence> absenceUnEtudiant = new HashMap<>();

        for (Absence absence : absences.values()) {
            if (absence.getEtudiant().getNom().equals(nom) && absence.getEtudiant().getPrenom().equals(prenom)) {
                absenceUnEtudiant.put(absence.returnKey(), absence);
                break;
            }
        }

//        if (absences.size() == 0) {
//            Absence absence = new Absence("", new Cours(new Date(), 420, "JAVA"), etudiant1);
//            absenceUnEtudiant.put(absence.returnKey(), absence);
//        }

        return absenceUnEtudiant;

    }
    
    //-recuperation de l'absence par le nom d'utilisateur
    @Override
     public Map<String, Absence> getAllAbsenceOneUser(String userName) {

        Map<String, Absence> absences = getAllAbsenceEtudiantDAO();
        Map<String, Absence> absenceUnEtudiant = new HashMap<>();

        for (Absence absence : absences.values()) {
            if (absence.getEtudiant().getLoginInfos().getLogin().equals(userName)) {
                absenceUnEtudiant.put(absence.returnKey(), absence);
                break;
            }
        }

        return absenceUnEtudiant;

    }
}
