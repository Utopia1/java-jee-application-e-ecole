package com.sqli.ecdf.model;

import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class MatiereTest {

    @Test
    public void matiereAddCoursTest() throws KeyAlreadyThereException {
        Cours cours1 = new Cours(new Date(), 420, "JAVA les bases");
        Cours cours2 = new Cours(new Date(), 380, "HTML les bases");
        Cours cours3 = new Cours(new Date(), 380, "MAVEN");
        Matiere matiere1 = new Matiere("JAVA JEE");
        matiere1.addCours(cours1);
        matiere1.addCours(cours2);
        matiere1.addCours(cours3);

        log.info("tableau de cours et matiere : {}", matiere1.getListCours() + matiere1.getNom());

        log.info("liste cours : {}", matiere1.getListCours());

    }
}