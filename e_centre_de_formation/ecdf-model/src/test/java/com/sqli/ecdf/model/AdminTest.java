package com.sqli.ecdf.model;

import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

public class AdminTest {
    
    @Test
    public void testCreatePersonne() throws KeyAlreadyThereException{
        Admin admin = new Admin("nom1", "prenom1", "email1","photo1", new Adresse("rue1", 75000, "Paris"), new LoginInfos("mdp1", "login1"));
        Personne personne = new Admin("nom1", "prenom1", "email1","photo1", new Adresse("rue1", 75000, "Paris"), new LoginInfos("mdp1", "login1"));
        Personne enseignant = new Enseignant("nom2", "prenom2", "email2","photo2", new Adresse("rue2", 75000, "Paris"), new LoginInfos("mdp2", "login2"));
        Personne etudiant = new Etudiant("nom3", "prenom3", "email3","photo3", new Adresse("rue3", 75000, "Paris"), new LoginInfos("mdp2", "login2"), new Date(2013 / 12 / 05));
        
        admin.addPersonne(personne);
        admin.addPersonne(enseignant);
        admin.addPersonne(etudiant);
        
    }
}