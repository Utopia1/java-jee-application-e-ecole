/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sqli.ecdf.model;

import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author stagiaire
 */
@Slf4j
public class EtudiantTest {
    

    @Test
    public void testIdentiteEtudiant() {
        
       Adresse adresse4 = new Adresse("rue4",12348,"toulon");
       LoginInfos login4 =new LoginInfos("mdp4","login5");
       Personne personne5 =new Etudiant("nom4","prenom4","eMail5","photo6",adresse4,login4,new Date(2013/10/05));
       
       log.info(personne5.toString());
       assertEquals("nom: nom4 prenom: prenom4 email: eMail5 photo: photo6 adresse: rue4 12348 toulon  LoginInfos mdp4 login5 date de naissande Thu Jan 01 01:00:00 CET 1970", personne5.toString());
    }
}