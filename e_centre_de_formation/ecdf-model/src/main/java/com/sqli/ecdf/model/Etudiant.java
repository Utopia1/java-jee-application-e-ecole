package com.sqli.ecdf.model;

import java.util.Date;
import lombok.Getter;

public class Etudiant extends Personne {

    @Getter
    private Date dateDeNaissance;

    public Etudiant(String nom, String prenom, String email, String photo, Adresse adresse, LoginInfos login, Date dateDeNaissance) {
        super(nom, prenom, email, photo, adresse, login);
        this.dateDeNaissance = dateDeNaissance;

    }
    
    @Override
    public String toString(){
        String result = super.toString() + " date de naissande " + dateDeNaissance;
        return result;
    }

    @Override
    public String returnType() {
        return "Etudiant";
    }
}
