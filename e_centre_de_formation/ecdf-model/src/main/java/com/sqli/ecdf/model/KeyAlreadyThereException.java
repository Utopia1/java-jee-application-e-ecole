package com.sqli.ecdf.model;

public class KeyAlreadyThereException extends Exception {

    public KeyAlreadyThereException(String message) {
        super(message);
    }
    
    @Override
    public String getMessage(){
        return "Key already there exception";
    }
}
