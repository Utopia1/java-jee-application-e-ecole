package com.sqli.ecdf.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Absence {

    private String motif;
    private Cours cours;
    private Etudiant etudiant;

    public String returnKey() {

        String result = motif + " " +cours.getDescription() + " " + etudiant.getNom()+" " + etudiant.getPhoto();
        return result;

    }
    
    public String getKey(){
        String result = motif + " " +cours.getDescription() + " " + etudiant.getNom()+" " + etudiant.getPhoto();
        return result;
    }
    
}
