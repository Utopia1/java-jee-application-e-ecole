package com.sqli.ecdf.model;

import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import com.sqli.ecdf.model.exception.KeyNotThereException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class Enseignant extends Personne {

    private Map<String, Enseigne> listEnseigne;

    public Enseignant(String nom, String prenom, String email, String photo, Adresse adresse, LoginInfos login) {
        super(nom, prenom, email, photo, adresse, login);
        listEnseigne = new HashMap<String, Enseigne>();
    }

    @Override
    public String returnType() {
        return "Enseignant";
    }
    
    
    public Set<Enseigne> getAllEnseignes(){
        Set<Enseigne> result = new HashSet<Enseigne>();
        for (Enseigne enseigne: listEnseigne.values()){
            result.add(enseigne);
        }
        return Collections.unmodifiableSet(result);
    }

    public void addEnseigne(Enseigne enseigne1) throws KeyAlreadyThereException {
        if(listEnseigne.containsKey(enseigne1.getMatiere().getNom())){
           throw new KeyAlreadyThereException("add enseigne failed !");
        }
        listEnseigne.put(enseigne1.getMatiere().getNom(), enseigne1);
    }

    public void removeEnseigne(Enseigne enseigne1) throws KeyNotThereException {
        if(!listEnseigne.containsKey(enseigne1.getMatiere().getNom())){
           throw new KeyNotThereException("remove enseigne failed !");
        }
        listEnseigne.remove(enseigne1.getMatiere().getNom());
    }

    public Enseigne getEnseigne(String nom) throws KeyNotThereException {
        if(!listEnseigne.containsKey(nom)){
           throw new KeyNotThereException("key doesn't exist !");
        }
        
        return listEnseigne.get(nom);
    }

    public void manageAbscences(Map<String, Absence> listAbscence) {
        for (Absence abs : listAbscence.values()) {
            log.info(abs.getMotif());
            log.info(abs.getCours().getDescription());
            log.info(abs.getEtudiant().getPrenom() + " " + abs.getEtudiant().getNom());
        }
    }
}
