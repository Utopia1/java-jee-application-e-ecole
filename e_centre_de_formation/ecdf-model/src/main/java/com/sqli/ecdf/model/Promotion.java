package com.sqli.ecdf.model;

import com.sqli.ecdf.model.exception.KeyNotThereException;
import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@Getter
@Setter
public class Promotion {
    
    private String libelle;
    private Map<String ,Etudiant> listEtudiant;
    private Map<String,Cours> listCours;

    public Set<Cours> getAllCours(){
        Set<Cours> result = new HashSet<Cours>();
        for (Cours cours : listCours.values()){
            result.add(cours);
        }
        return result;
    }
    
    public Set<Etudiant> getAllEtudiants(){
        Set<Etudiant> result = new HashSet<Etudiant>();
        for (Etudiant etudiant : listEtudiant.values()){
            result.add(etudiant);
        }
        
        return Collections.unmodifiableSet(result);
        
    }
    
    public Promotion(String libelle) {
        this.libelle = libelle;
        listEtudiant = new HashMap<String, Etudiant>();
        listCours = new HashMap<String,Cours>();
    }
    
    public void addEtudiant(Etudiant etudiant) throws Exception{
        if(listEtudiant.containsKey(etudiant.returnKey())){
            throw new KeyAlreadyThereException("add etudiant failed !");
        }
        listEtudiant.put(etudiant.returnKey(), etudiant);
    }
    public void removeEtudiant(Etudiant etudiant) throws Exception{
        if(!listEtudiant.containsKey(etudiant.returnKey())){
            throw new KeyNotThereException ("remove etudiant failed !");
        }
        listEtudiant.remove(etudiant.returnKey());
        
    }
    public Etudiant getEtudiant(String key ) throws Exception{
        if(!listEtudiant.containsKey(key)){
            throw new KeyNotThereException ("remove etudiant failed !");
        }
        return listEtudiant.get(key);
    }
    
    public void addCours(Cours cours){
        listCours.put(cours.getDescription(), cours);
    }
    
    public void removeCours(String nom ){
        listCours.remove(nom);
        
    }
    public Cours getCours(String key){
        return listCours.get(key);
    }

            
}
