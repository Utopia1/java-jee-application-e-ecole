package com.sqli.ecdf.model;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Cours {

    private Date date;
    private int dureeEnMinutes;
    private String description;
    
    
}
