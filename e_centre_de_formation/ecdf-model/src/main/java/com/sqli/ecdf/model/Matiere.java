package com.sqli.ecdf.model;

import com.sqli.ecdf.model.exception.KeyNotThereException;
import com.sqli.ecdf.model.exception.KeyAlreadyThereException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import lombok.Getter;

public class Matiere {

    @Getter
    private String nom;
    @Getter
    private Map<String, Cours> listCours;
    
    

    public Matiere(String nom) {
        this.nom = nom;
        listCours = new HashMap<String, Cours>();
    }
    
    public Set<Cours> getAllCours(){
        Set<Cours> result = new HashSet<Cours>();
        for (Cours cours : listCours.values()){
            result.add(cours);
        }
        return Collections.unmodifiableSet(result);
    }
    
    public Set<Cours> getOneMatiereCours(String nom){
        Set<Cours> result = new HashSet<Cours>();
        for (Cours cours : listCours.values()){
            if(this.getNom().equals(nom)){
            result.add(cours);
            }
        }
        return Collections.unmodifiableSet(result);
    }

    public void addCours(Cours cours) throws KeyAlreadyThereException {
        if (listCours.containsKey(cours.getDescription())) {
            throw new KeyAlreadyThereException("add cours failed !");
        }
        listCours.put(cours.getDescription(), cours);
    }

    public void removeCours(Cours cours) throws KeyNotThereException {
        if (!listCours.containsKey(cours.getDescription())) {
            throw new KeyNotThereException("remove cours failed !");
        }
        listCours.remove(cours.getDescription());
    }

    public Cours getCours(String description) throws KeyNotThereException {
        if (!listCours.containsKey(description)) {
            throw new KeyNotThereException("cours doesn't exist !");
        }
        return listCours.get(description);
    }
    
}
