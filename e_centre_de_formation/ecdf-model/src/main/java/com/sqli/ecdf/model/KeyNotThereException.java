package com.sqli.ecdf.model;

public class KeyNotThereException extends Exception {

    public KeyNotThereException(String message) {
        super(message);
    }
    
    @Override
    public String getMessage(){
        return "Key not there exception";
    }
}
