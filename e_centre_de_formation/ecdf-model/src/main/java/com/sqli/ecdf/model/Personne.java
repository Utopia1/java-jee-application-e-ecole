package com.sqli.ecdf.model;

import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"nom", "prenom"})
public class Personne {

    private String nom;
    private String prenom;
    private String email;
    private String photo;
    private Adresse adresse;
    private LoginInfos loginInfos;

    @Override
    public String toString() {
        return "nom: " + this.nom + " prenom: " + this.prenom + " email: " + this.email + " photo: " + this.photo + " adresse: "
                + this.adresse.rue + " " + this.adresse.codePostal + " " + this.adresse.ville + " " + " LoginInfos " + this.loginInfos.motDePasse + " " + this.loginInfos.login;
    }

    public String returnKey() {

        return nom + "_" + prenom + "-" + returnType();
    }

    public String returnType() {
        return "Personne";
    }

    public String getLogin() {
        return loginInfos.login;
    }

    public String getMotDePasse() {
        return loginInfos.motDePasse;
    }

}
